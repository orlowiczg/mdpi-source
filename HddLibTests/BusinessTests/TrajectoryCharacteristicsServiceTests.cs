using NUnit.Framework;
using HddLib.Models;
using HddLib.Services;

namespace Tests
{
    [TestFixture]
    public class Tests
    {


        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void BusinessLogic1()
        {
            Input2D input = new Input2D();
            input.A = 920;
            input.H = 1.10323;
            input.L1 = 160;
            input.Angle[1] = -10;
            input.Angle[3] = 0;
            input.Angle[5] = 7;
            input.R[2] = 1250;
            input.R[4] = 1250;
            TrajectoryCharacteristics trajectoryCharacteristics = TrajectoryCharacteristicsService.GetLRLRL(input);

            Assert.AreEqual(-27.7837084267089, trajectoryCharacteristics.H[1], 0.0001);
            Assert.AreEqual(157.5692405, trajectoryCharacteristics.A[1], 0.0001);
            Assert.AreEqual(-18.99030873, trajectoryCharacteristics.H[2], 0.0001);
            Assert.AreEqual(0.045836624, trajectoryCharacteristics.DLS[2], 0.0001);
            Assert.AreEqual(218.1661565, trajectoryCharacteristics.L[2], 0.0001);
            Assert.AreEqual(9.317310448, trajectoryCharacteristics.H[4], 0.0001);
            Assert.AreEqual(152.3366793, trajectoryCharacteristics.A[4], 0.0001);
            Assert.AreEqual(0.045836624, trajectoryCharacteristics.DLS[4], 0.0001);
            Assert.AreEqual(152.7163095, trajectoryCharacteristics.L[4], 0.0001);
            Assert.AreEqual(78.98837535, trajectoryCharacteristics.L[3], 0.0001);
            Assert.AreEqual(0, trajectoryCharacteristics.H[3], 0.0001);
            Assert.AreEqual(78.98837535, trajectoryCharacteristics.A[3], 0.0001);
            Assert.AreEqual(38.55993671, trajectoryCharacteristics.H[5], 0.0001);
            Assert.AreEqual(314.0454828, trajectoryCharacteristics.A[5], 0.0001);
            Assert.AreEqual(316.4039096, trajectoryCharacteristics.L[5], 0.0001);
            Assert.AreEqual(926.274751, trajectoryCharacteristics.L[0], 0.0001);
        }
    }
}
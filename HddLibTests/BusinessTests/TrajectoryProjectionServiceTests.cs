using System;
using System.Collections.Generic;
using System.Diagnostics;
using HddLib.Models;
using HddLib.Services;
using HddLib.Tools;
using NUnit.Framework;


namespace Tests
{
    [TestFixture]
    public class TrajectoryProjectionServiceTests
    {
        [Test]
        public void BusinessLogic1()
        {
            Input2D inputParameters = new Input2D();
            inputParameters.A = 920;
            inputParameters.H = 1.10323;
            inputParameters.L1 = 160;
            inputParameters.Angle[1] = -10;
            inputParameters.Angle[3] = 0;
            inputParameters.Angle[5] = 7;
            inputParameters.R[2] = 1250;
            inputParameters.R[4] = 1250;
            TrajectoryCharacteristics trajectoryCharacteristics = TrajectoryCharacteristicsService.GetLRLRL(inputParameters);
            TrajectoryProjection trajectoryProjection = TrajectoryProjectionService.GetTrajectory2DbyMD(trajectoryCharacteristics);

            //Results review
            Console.WriteLine(inputParameters.ToString());
            Console.WriteLine(trajectoryCharacteristics.ToString());
            Console.WriteLine(trajectoryProjection.ToString());
        }

        [Test]
        public void BusinessLogic2()
        {
            Input2D inputParameters = new Input2D();
            inputParameters.A = 801.15;
            inputParameters.H = -1.103226;
            inputParameters.L1 = 260;
            inputParameters.Angle[1] = -7;
            inputParameters.Angle[3] = 0;
            inputParameters.Angle[5] = 9.877564519;
            inputParameters.R[2] = 1250;
            inputParameters.R[4] = 1250;
            TrajectoryCharacteristics trajectoryCharacteristics = TrajectoryCharacteristicsService.GetLRLRL(inputParameters);
            TrajectoryProjection trajectoryProjection = TrajectoryProjectionService.GetTrajectory2DbyMD(trajectoryCharacteristics);

            //Results review
            Console.WriteLine(inputParameters.ToString());
            Console.WriteLine(trajectoryCharacteristics.ToString());
            Console.WriteLine(trajectoryProjection.ToString());
        }

        [Test]
        public void BusinessLogic3()
        {
            Input2D inputParameters = new Input2D();
            inputParameters.A = 801.15;
            inputParameters.H = -1.103226;
            inputParameters.L1 = 260;
            inputParameters.Angle[1] = -7;
            inputParameters.Angle[3] = 0;
            inputParameters.Angle[5] = 9.877564519;
            inputParameters.R[2] = 1250;
            inputParameters.R[4] = 1250;
            TrajectoryCharacteristics trajectoryCharacteristics = TrajectoryCharacteristicsService.GetLRLRL(inputParameters);
            TrajectoryProjection trajectoryProjection = TrajectoryProjectionService.GetTrajectory2DbyMD(trajectoryCharacteristics);

            //Results review
            Console.WriteLine(inputParameters.ToString());
            Console.WriteLine(trajectoryCharacteristics.ToString());
            Console.WriteLine(trajectoryProjection.ToString());
        }

        [Test]
        public void GetCatanary2DbyX()
        {
            var catenaryCharacteristics = new TrajectoryCharacteristics();
            catenaryCharacteristics.A[0] = 1000;
            catenaryCharacteristics.H[0] = -15;
            double q = 700;
            double Npoz = 9.81 * 250000;

            TrajectoryProjection catenaryProjection = TrajectoryProjectionService.GetCatenary2DbyX(catenaryCharacteristics, 0, q, Npoz, 50);

            Console.WriteLine(catenaryProjection.ToString());
        }

        [Test]
        public void GetSectional2DbyX()
        {
            var inputParameters = new Input2D();
            inputParameters.A = 1000;
            inputParameters.H = -15;
            inputParameters.L1 = 120;
            inputParameters.Angle[1] = -10;
            inputParameters.Angle[3] = 0;
            inputParameters.Angle[5] = 7;
            inputParameters.R[2] = 1250;
            inputParameters.R[4] = 1250;

            TrajectoryProjection trajectoryProjection = TrajectoryProjectionService.GetTrajectory2DbyX(TrajectoryCharacteristicsService.GetLRLRL(inputParameters));

            Console.WriteLine(trajectoryProjection.ToString());
        }

        [Test]
        public void PrintCatenaryAnd3x2d()
        {
            var catenaryCharacteristics = new TrajectoryCharacteristics();
            catenaryCharacteristics.A[0] = 1000;
            catenaryCharacteristics.H[0] = -25;
            double q = 800;
            double Npoz = 9.81 * 250000;

            TrajectoryProjection catenaryProjection = TrajectoryProjectionService.GetCatenary2DbyX(catenaryCharacteristics, 0, q, Npoz);

            var inputParameters = new Input2D();
            inputParameters.A = 1000;
            inputParameters.H = -25;
            inputParameters.L1 = 75.1;
            inputParameters.Angle[1] = -9.07;
            inputParameters.Angle[3] = 0;
            inputParameters.Angle[5] = 8.01;
            inputParameters.R[2] = 2933;
            inputParameters.R[4] = 2988;
            TrajectoryProjection trajectoryProjection, result;
            TrajectoryCharacteristics trajectoryCharacteristics;
            double sos;

            trajectoryCharacteristics = TrajectoryCharacteristicsService.GetLRLRL(inputParameters);
            result = TrajectoryProjectionService.GetTrajectory2DbyX(trajectoryCharacteristics);
            sos = Extensions.SumOfSquaresInterpolated(catenaryProjection, result);
            Console.WriteLine($"Sum of squares: {sos}");

            trajectoryProjection = TrajectoryProjectionService.GetTrajectory2DbyX(trajectoryCharacteristics, 50);
            catenaryProjection = TrajectoryProjectionService.GetCatenary2DbyX(catenaryCharacteristics, 0, q, Npoz, 50);
            Console.WriteLine(trajectoryProjection.ToString());
            Console.WriteLine(trajectoryCharacteristics.ToString());
        }
    }
}
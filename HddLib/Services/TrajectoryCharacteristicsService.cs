using HddLib.Models;
using System;
using HddLib.Tools;

namespace HddLib.Services
{
    public class TrajectoryCharacteristicsService
    {
        public static TrajectoryCharacteristics GetLRLRL(Input2D _in)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Input not initialized");
            }
            else if (_in.A <= 0)
            {
                throw new Exception("A must be greater than 0");
            }
            else if (_in.L1 <= 0)
            {
                throw new Exception("In this case L1 must be greater than 0");
            }
            else if (_in.R[2] <= 0 | _in.R[4] <= 0)
            {
                throw new Exception("In this case R2 must be negative and R4 must be positive");
            }
            else if (_in.Angle[1] >= 0 | _in.Angle[5] <= 0)
            {
                throw new Exception("In this case Angle1 and Angle5 must be greater than 0");
            }
            else if (_in.Angle[3] != 0)
            {
                throw new Exception("In this case Angle3 must be 0");
            }

            TrajectoryCharacteristics _out = new TrajectoryCharacteristics();
            // 1st section - linear
            _out.H[1] = _in.L1 * Math.Sin(_in.Angle[1].ToRadian());
            _out.A[1] = _in.L1 * Math.Cos(_in.Angle[1].ToRadian());
            _out.L[1] = _in.L1;

            // 2nd section - curved
            _out.H[2] = _in.R[2] * (Math.Cos(_in.Angle[1].ToRadian()) - Math.Cos(_in.Angle[3].ToRadian()));
            _out.A[2] = _in.R[2] * (Math.Sin(_in.Angle[3].ToRadian()) - Math.Sin(_in.Angle[1].ToRadian()));
            _out.DLS[2] = 180 / Math.PI / _in.R[2];
            //deltaj = epsj - epsj-1
            _out.L[2] = (_in.Angle[3] - _in.Angle[1]).ToRadian() * _in.R[2];

            // 3rd section - linear horizontal
            _out.L[3] = (
                _in.A * Math.Sin(_in.Angle[5].ToRadian())
                - _in.H * Math.Cos(_in.Angle[5].ToRadian())
                - _in.L1 * Math.Sin(_in.Angle[5].ToRadian() - _in.Angle[1].ToRadian())
                + _in.R[2] * (Math.Cos(_in.Angle[5].ToRadian() - _in.Angle[1].ToRadian()) - Math.Cos(_in.Angle[5].ToRadian() - _in.Angle[3].ToRadian()))
                - _in.R[4] * (1 - Math.Cos(_in.Angle[5].ToRadian() - _in.Angle[3].ToRadian()))
            )
            / (Math.Sin(_in.Angle[5].ToRadian() - _in.Angle[3]));
            _out.H[3] = _out.L[3] * Math.Sin(_in.Angle[3].ToRadian());
            _out.A[3] = _out.L[3] * Math.Cos(_in.Angle[3].ToRadian());

            // 4th section - curved
            _out.H[4] = _in.R[4] * (Math.Cos(_in.Angle[3].ToRadian()) - Math.Cos(_in.Angle[5].ToRadian()));
            _out.A[4] = _in.R[4] * (Math.Sin(_in.Angle[5].ToRadian()) - Math.Sin(_in.Angle[3].ToRadian()));
            _out.DLS[4] = 180 / Math.PI / _in.R[4];
            //deltaj = epsj - epsj-1
            _out.L[4] = (_in.Angle[5] - _in.Angle[3]).ToRadian() * _in.R[4];

            // 5th section - linear
            _out.H[5] = _in.H - _out.H[4] - _out.H[3] - _out.H[2] - _out.H[1];
            _out.A[5] = _in.A - _out.A[4] - _out.A[3] - _out.A[2] - _out.A[1];
            _out.L[5] = Math.Sqrt(_out.H[5] * _out.H[5] + _out.A[5] * _out.A[5]);

            // sums
            _out.L[0] = _out.L[1] + _out.L[2] + _out.L[3] + _out.L[4] + _out.L[5];
            _out.H[0] = _out.H[1] + _out.H[2] + _out.H[3] + _out.H[4] + _out.H[5];
            _out.A[0] = _out.A[1] + _out.A[2] + _out.A[3] + _out.A[4] + _out.A[5];

            // 
            Array.Copy(_in.Angle, _out.Angle, 6);
            Array.Copy(_in.R, _out.R, 6);

            return _out;
        }
    }
}
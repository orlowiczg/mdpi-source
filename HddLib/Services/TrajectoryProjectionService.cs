using System;
using HddLib.Models;
using HddLib.Tools;

namespace HddLib.Services
{
    public class TrajectoryProjectionService
    {
        public static TrajectoryProjection GetTrajectory2DbyMD(TrajectoryCharacteristics _in, int defaultStep = 10)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Not initialized input");
            }

            double Hsum = 0;
            double Asum = 0;
            double Lsum = 0;
            double LL = 0;
            int LS = 0;
            int correction = 0;
            int step = defaultStep;
            int step_model = step;
            double azimuth = 0;
            var _out = new TrajectoryProjection();

            for (int section = 1; section <= 5; section++)
            {
                step = defaultStep;
                while (LL < Lsum + _in.L[section])
                {
                    _out.points.Add(new PointGeometry());
                    // linear section __ R = 0
                    if (_in.R[section] == 0)
                    {
                        double alfaSection = _in.Angle[section].ToRadian();
                        _out[LS].MD = LL;
                        _out[LS].Alfa = _in.Angle[section].ToRadian(); ;
                        _out[LS].X = (Asum + (LL - Lsum) * Math.Cos(alfaSection)) * Math.Sin(azimuth);
                        _out[LS].Y = (Asum + (LL - Lsum) * Math.Cos(alfaSection)) * Math.Cos(azimuth);
                        _out[LS].Z = Hsum + (LL - Lsum) * Math.Sin(alfaSection);
                    }
                    // curved section \_ ej>ej-1
                    else if (_in.Angle[section] > _in.Angle[section - 1])
                    {
                        double alfaSection = _in.Angle[section - 1].ToRadian();
                        _out[LS].MD = LL;
                        _out[LS].Alfa = alfaSection + ((LL - Lsum) * _in.DLS[section]).ToRadian();
                        _out[LS].X = (Asum + _in.R[section] * (Math.Sin(_out[LS].Alfa) - Math.Sin(alfaSection))) * Math.Sin(azimuth);
                        _out[LS].Y = (Asum + _in.R[section] * (Math.Sin(_out[LS].Alfa) - Math.Sin(alfaSection))) * Math.Cos(azimuth);
                        _out[LS].Z = Hsum + _in.R[section] * (Math.Cos(alfaSection) - Math.Cos(_out[LS].Alfa));
                    }
                    // curved section _/ ej<ej-1
                    else
                    {
                        double alfaSection = _in.Angle[section - 1].ToRadian();
                        _out[LS].MD = LL;
                        _out[LS].Alfa = alfaSection + ((LL - Lsum) * _in.DLS[section]).ToRadian();
                        _out[LS].X = (Asum + _in.R[section] * (Math.Sin(alfaSection) - Math.Sin(_out[LS].Alfa))) * Math.Sin(azimuth);
                        _out[LS].Y = (Asum - _in.R[section] * (Math.Sin(alfaSection) - Math.Sin(_out[LS].Alfa))) * Math.Cos(azimuth);
                        _out[LS].Z = Hsum + _in.R[section] * (Math.Cos(alfaSection) - Math.Cos(_out[LS].Alfa));
                    }

                    if (correction == 1)
                        step_model = step;
                    LS += 1;
                    LL += step;
                    correction = 0;
                } //while

                _out.points.Add(new PointGeometry());
                _out[LS].MD = Lsum + _in.L[section];
                _out[LS].Alfa = _in.Angle[section].ToRadian();
                _out[LS].X = (Asum + _in.A[section]) * Math.Sin(azimuth);
                _out[LS].Y = (Asum + _in.A[section]) * Math.Cos(azimuth);
                _out[LS].Z = Hsum + _in.H[section];

                step = (int)(LL - (Lsum + _in.L[section]));
                if (step == 0)
                {
                    correction = 0;
                    step = step_model;
                    LL += step;
                }
                else correction = 1;

                LS = LS + 1;
                Hsum += _in.H[section];
                Asum += _in.A[section];
                Lsum += _in.L[section];
            }
            return _out;
        }
        public static TrajectoryProjection GetCatenary(TrajectoryCharacteristics _in, double azimuth, double q)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Not initialized input");
            }

            var _out = new TrajectoryProjection();
            double beta = azimuth.ToRadian();
            double Npoz = Extensions.NpozBisection(_in.A[0], _in.H[0], -10, q, 1, 1000000000, 0.01);
            double Lk = Npoz / q * Math.Sinh(q / Npoz * _in.A[0]);

            for (int LL = 0, i = 0; LL < Lk; LL = LL + 10, i++)
            {
                _out.points.Add(new PointGeometry());

                _out[i].MD = LL;
                _out[i].Alfa = Math.Atan(q / Npoz * LL + Math.Sinh(q / Npoz * (-_in.A[0])));
                _out[i].X = (Npoz / q * Extensions.Asinh(q / Npoz * LL + Math.Sinh(q / Npoz * (-_in.A[0]))) + _in.A[0]) * Math.Sin(beta);
                _out[i].Y = (Npoz / q * Extensions.Asinh(q / Npoz * LL + Math.Sinh(q / Npoz * (-_in.A[0]))) + _in.A[0]) * Math.Cos(beta);
                _out[i].Z = Npoz / q * Math.Cosh(Extensions.Asinh(q / Npoz * LL + Math.Sinh(q / Npoz * (-_in.A[0])))) - Npoz / q * Math.Cosh(q / Npoz * (-_in.A[0]));
            }
            return _out;
        }
        public static TrajectoryProjection GetCatenary(TrajectoryCharacteristics _in, double azimuth, double q, double inc)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Not initialized input");
            }
            else if (_in.A[0] <= 0)
            {
                throw new ArgumentException("A[0] invalid value");
            }

            var _out = new TrajectoryProjection();
            double beta = azimuth.ToRadian();
            double Npoz = Extensions.NpozBisection(_in.A[0], _in.H[0], inc, q, 1, 1000000000, 0.01);
            double Lk = Npoz / q * Math.Sinh(q / Npoz * _in.A[0] + Extensions.Asinh(Math.Tan(inc.ToRadian()))) - Npoz / q * Math.Tan(inc.ToRadian());

            for (int LL = 0, i = 0; LL < Lk; LL = LL + 10, i++)
            {
                _out.points.Add(new PointGeometry());

                _out[i].MD = LL;
                _out[i].Alfa = Math.Atan(q / Npoz * LL + Math.Tan(inc.ToRadian()));
                _out[i].Beta = beta;
                double nl = Npoz / q * Extensions.Asinh(q / Npoz * LL + Math.Tan(inc.ToRadian())) - Npoz / q * Extensions.Asinh(Math.Tan(inc.ToRadian()));
                _out[i].X = nl * Math.Sin(beta);
                _out[i].Y = nl * Math.Cos(beta);
                _out[i].Z = Npoz / q * Math.Cosh(Extensions.Asinh(q / Npoz * LL + Math.Tan(inc.ToRadian()))) - Npoz / q * Math.Cosh(Extensions.Asinh(Math.Tan(inc.ToRadian())));
            }

            // last point verification
            if (Lk != _out[_out.Count - 1].MD)
            {
                double nl = Npoz / q * Extensions.Asinh(q / Npoz * Lk + Math.Tan(inc.ToRadian())) - Npoz / q * Extensions.Asinh(Math.Tan(inc.ToRadian()));
                _out.points.Add(new PointGeometry()
                {
                    MD = Lk,
                    Alfa = Math.Atan(q / Npoz * Lk + Math.Tan(inc.ToRadian())),
                    Beta = beta,
                    X = nl * Math.Sin(beta),
                    Y = nl * Math.Cos(beta),
                    Z = Npoz / q * Math.Cosh(Extensions.Asinh(q / Npoz * Lk + Math.Tan(inc.ToRadian()))) - Npoz / q * Math.Cosh(Extensions.Asinh(Math.Tan(inc.ToRadian())))
                });
            }
            return _out;
        }
        public static TrajectoryProjection GetTrajectory2DbyX(TrajectoryCharacteristics _in, int defaultStep = 10)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Not initialized input");
            }

            double Hsum = 0;
            double Asum = 0;
            double alfaSection = 0;
            int x = 0;
            int i = 0;
            var _out = new TrajectoryProjection();
            _out.trajectoryCharacteristics = _in;

            for (int section = 1; section <= 5; section++)
            {
                int limes = (int)Math.Ceiling(Asum + _in.A[section]);
                while (x <= limes)
                {
                    _out.points.Add(new PointGeometry());
                    _out[i].X = x;
                    if (_in.R[section] == 0)
                    {
                        alfaSection = _in.Angle[section].ToRadian();
                        _out[i].Alfa = _in.Angle[section].ToRadian(); ;
                        _out[i].Y = Hsum + (x - Asum) * Math.Tan(alfaSection);
                    }
                    else
                    {
                        alfaSection = -_in.Angle[section - 1].ToRadian();
                        _out[i].Alfa = -Math.Asin(Math.Sin(alfaSection) - (x - Asum) / _in.R[section]);
                        _out[i].Y = Hsum + _in.R[section] * (-Math.Cos(_out[i].Alfa) + Math.Cos(alfaSection));
                    }
                    x += defaultStep;
                    i++;
                }

                Hsum += _in.H[section];
                Asum += _in.A[section];
            }
            return _out;
        }
        public static TrajectoryProjection GetCatenary2DbyX(TrajectoryCharacteristics _in, double azimuth, double q, double Npoz, int defaultStep = 10)
        {
            if (_in == null)
            {
                throw new NullReferenceException("Not initialized input");
            }

            var _out = new TrajectoryProjection();
            _out.trajectoryCharacteristics = _in;
            int x = 0;
            int i = 0;
            double c1 = TrajectoryProjectionService.c1Bisection(_in.A[0], _in.H[0], Npoz, q);
            double eps0 = Math.Atan(Math.Sinh(q / Npoz * c1)).ToDeg();

            while (x <= _in.A[0])
            {
                _out.points.Add(new PointGeometry());
                _out[i].X = x;
                _out[i].Y = CatenaryPsiHorizontal(q, eps0, x, Npoz);
                _out[i].Alfa = Math.Atan(q / Npoz * x + Math.Tan(eps0.ToRadian()));
                _out[i].MD = Npoz / q * Math.Sinh(q / Npoz * x + Extensions.Asinh(Math.Tan(eps0.ToRadian()))) - Npoz / q * Math.Tan(eps0.ToRadian());
                x += defaultStep;
                i++;
            }

            return _out;
        }

        private static double CatenaryPsiHorizontal(double q, double inc, int x, double Npoz)
        {
            return Npoz / q * Math.Cosh((q / Npoz) * x + Extensions.Asinh(Math.Tan(inc.ToRadian()))) - (Npoz / q) * Math.Cosh(Extensions.Asinh(Math.Tan(inc.ToRadian())));
        }

        public static double c1Bisection(double Ak, double Hk, double npoz, double q, double a = -10000000, double b = 0, double epsilon = 0.01)
        {
            if (gFunct(npoz, a, q, Ak, Hk) * gFunct(npoz, b, q, Ak, Hk) > 0)
            {
                throw new Exception("Function has the same signs at ends of interval");
            }

            double xsr = 0;
            double gsr = 0;
            double ga = 0;

            while (Math.Abs(a - b) > epsilon)
            {
                xsr = (a + b) / 2.0;
                gsr = gFunct(npoz, xsr, q, Ak, Hk);
                ga = gFunct(npoz, a, q, Ak, Hk);

                if ((ga * gsr) < 0)
                {
                    b = xsr;
                }
                else
                {
                    a = xsr;
                }
            }
            return (a + b) / 2;
        }

        private static double gFuncVertical(double x, double q, double Ak, double Hk)
        {
            return 1.0 * x / q - (1.0 * x / q) * Math.Cosh((1.0 * q / x) * (-Ak)) + Hk;
        }
        private static double gFunctHorizontal(double inc, double x, double q, double Ak, double Hk)
        {
            return 1.0 * x / q * Math.Cosh(q / x * Ak + Extensions.Asinh(Math.Tan(inc.ToRadian()))) - x / q * Math.Cosh(Extensions.Asinh(Math.Tan(inc.ToRadian()))) - Hk;
        }
        private static double gFunct(double npoz, double x, double q, double Ak, double Hk)
        {
            return 1.0 * Math.Cosh(q / npoz * (Ak + x)) - Math.Cosh(q / npoz * x) - q / npoz * Hk;
        }
    }
}
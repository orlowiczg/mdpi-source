using System;
using HddLib.Models;

namespace HddLib.Tools
{
    public static class Extensions
    {
        public static double ToRadian(this double input)
        {
            return input * Math.PI / 180.0;
        }

        public static double ToDeg(this double input)
        {
            return input * 180.0 / Math.PI;
        }

        public static double To2(this double input)
        {
            return Math.Round(input, 2);
        }

        public static double Asinh(double x)
        {
            return Math.Log(x + Math.Sqrt(x * x + 1));
        }

        public static double SumOfSquaresInterpolated(TrajectoryProjection firstTrajectoryProjection, TrajectoryProjection secondTrajectoryProjection)
        {
            double sum = 0;
            int lim = 0;
            if (firstTrajectoryProjection.Count > secondTrajectoryProjection.Count)
            {
                lim = secondTrajectoryProjection.Count;
            }
            else
                lim = firstTrajectoryProjection.Count;

            for (int i = 0; i < lim; i++)
            {
                sum = sum + (firstTrajectoryProjection[i].Y - secondTrajectoryProjection[i].Y) * (firstTrajectoryProjection[i].Y - secondTrajectoryProjection[i].Y);
            }
            return sum;
        }

        public static float SumOfSquaresInterpolatedFloat(TrajectoryProjection firstTrajectoryProjection, TrajectoryProjection secondTrajectoryProjection)
        {
            float sum = 0f;
            int lim = 0;
            if (firstTrajectoryProjection.Count > secondTrajectoryProjection.Count)
            {
                lim = secondTrajectoryProjection.Count;
            }
            else
                lim = firstTrajectoryProjection.Count;

            for (int i = 0; i < lim; i++)
            {
                sum = sum + (float)((firstTrajectoryProjection[i].Y - secondTrajectoryProjection[i].Y) * (firstTrajectoryProjection[i].Y - secondTrajectoryProjection[i].Y));
            }
            return sum;
        }

        public static LinearFunction CatenaryTangent(TrajectoryCharacteristics characteristics, double q, double inc, int x0)
        {
            double Npoz = NpozBisection(characteristics.A[0], characteristics.H[0], inc, q, 1, 1000000000, 0.01);

            double fx0 = CatenaryPsi(q, inc, x0, Npoz);
            double fprimx0 = Math.Sinh((q / Npoz) * x0 + Extensions.Asinh(Math.Tan(inc.ToRadian())));
            var result = new LinearFunction();
            result.A = fprimx0;
            result.B = fx0 - fprimx0 * x0;
            result.Slope = Math.Atan(fprimx0).ToDeg();

            return result;
        }
        private static double CatenaryPsi(double q, double inc, int x, double Npoz)
        {
            return Npoz / q * Math.Cosh((q / Npoz) * x + Extensions.Asinh(Math.Tan(inc.ToRadian()))) - (Npoz / q) * Math.Cosh(Extensions.Asinh(Math.Tan(inc.ToRadian())));
        }

        public static double NpozBisection(double Ak, double Hk, double inc, double q, double a, double b, double epsilon)
        {
            if (gfunc1(inc, a, q, Ak, Hk) * gfunc1(inc, b, q, Ak, Hk) > 0)
            {
                throw new Exception("Function has the same signs at ends of interval");
            }

            double xsr = 0;
            double gsr = 0;
            double ga = 0;

            while (Math.Abs(a - b) > epsilon)
            {
                xsr = (a + b) / 2.0;
                gsr = gfunc1(inc, xsr, q, Ak, Hk);
                ga = gfunc1(inc, a, q, Ak, Hk);

                if ((ga * gsr) < 0)
                {
                    b = xsr;
                }
                else
                {
                    a = xsr;
                }
            }
            return (a + b) / 2;
        }

        private static double gfunc(double x, double q, double Ak, double Hk)
        {
            return 1.0 * x / q - (1.0 * x / q) * Math.Cosh((1.0 * q / x) * (-Ak)) + Hk;
        }

        private static double gfunc1(double inc, double x, double q, double Ak, double Hk)
        {
            return 1.0 * x / q * Math.Cosh(q / x * Ak + Asinh(Math.Tan(inc.ToRadian()))) - x / q * Math.Cosh(Asinh(Math.Tan(inc.ToRadian()))) - Hk;
        }
    }
}
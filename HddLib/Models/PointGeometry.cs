using HddLib.Tools;
namespace HddLib.Models
{
    public class PointGeometry
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }
        public double Alfa { get; set; }
        public double Beta { get; set; }
        public double MD { get; set; }

        public override string ToString()
        {
            return $"{MD.To2()}\t{X.To2()}\t{Y.To2()}\t{Z.To2()}\t{Alfa.ToDeg().To2()}\t{Beta.ToDeg().To2()}";
        }
        public string Label()
        {
            return "MD\tX\tY\tZ\tAlfa\tBeta";
        }
    }
}
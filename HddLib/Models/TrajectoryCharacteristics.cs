using HddLib.Tools;
namespace HddLib.Models
{
    public class TrajectoryCharacteristics
    {
        public string Name { get; set; }
        public double[] A { get; set; } = new double[6];
        public double[] H { get; set; } = new double[6];
        public double[] L { get; set; } = new double[6];
        public double[] DLS { get; set; } = new double[6];
        public double[] R { get; set; } = new double[6];
        public double[] Angle { get; set; } = new double[6];

        public override string ToString()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder("Trajectory characteristics\n[i]\tA\tH\tL\tR\tAngle\n");
            for (int i = 0; i < 6; i++)
            {
                result.AppendLine($"{i}\t{this.A[i].To2()}\t{this.H[i].To2()}\t{this.L[i].To2()}\t{this.R[i].To2()}\t{this.Angle[i].To2()}\t");
            }
            return result.ToString();
        }
    }
}
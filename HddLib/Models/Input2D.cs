namespace HddLib.Models
{
    public class Input2D
    {
        public string Name { get; set; }
        public double A { get; set; }
        public double H { get; set; }
        public double L1 { get; set; }
        public double[] R { get; set; } = new double[6];
        public double[] Angle { get; set; } = new double[6];

        public override string ToString()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder("\nInput parameters\nA\tH\tL\n");
            result.AppendLine($"{A}\t{H}\t{L1}\n[i]\tR\tAngle");
            for (int i = 1; i < 6; i++)
            {
                result.AppendLine($"{i}\t{R[i]}\t{Angle[i]}");
            }

            return result.ToString();
        }

    }
}
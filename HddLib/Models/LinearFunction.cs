namespace HddLib.Models
{
    public struct LinearFunction
    {
        public double A { get; set; }
        public double B { get; set; }
        public double Slope { get; set; }
    }
}
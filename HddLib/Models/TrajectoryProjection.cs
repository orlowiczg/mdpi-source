using System.Collections;
using System.Collections.Generic;

namespace HddLib.Models
{
    public class TrajectoryProjection : IEnumerable<PointGeometry>
    {
        public List<PointGeometry> points { get; set; } = new List<PointGeometry>();
        public TrajectoryCharacteristics trajectoryCharacteristics;

        public int Count
        {
            get
            {
                return points.Count;
            }
        }
        public PointGeometry this[int index]
        {
            get { return points[index]; }
            set { points.Insert(index, value); }
        }

        public double[] GetColumn(Column column)
        {
            List<double> temp = new List<double>();

            switch ((int)column)
            {
                case 1:
                    foreach (var point in points)
                    {
                        temp.Add(point.X);
                    }
                    break;
                case 2:
                    foreach (var point in points)
                    {
                        temp.Add(point.Y);
                    }
                    break;
                case 3:
                    foreach (var point in points)
                    {
                        temp.Add(point.Z);
                    }
                    break;
                default:
                    return null;
            }
            return temp.ToArray();
        }
        public IEnumerator<PointGeometry> GetEnumerator()
        {
            return points.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        public override string ToString()
        {
            System.Text.StringBuilder result = new System.Text.StringBuilder("Trajectory projection\n");
            result.AppendLine($"[i]\t{points[0].Label()}");
            int i = 0;
            foreach (var item in points)
            {
                result.AppendLine($"{i++}\t{item.ToString()}");
            }
            return result.ToString();
        }

        public enum Column { X = 1, Y = 2, Z = 3 }

    }
}